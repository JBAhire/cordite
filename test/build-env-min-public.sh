#!/bin/bash
#
#   Copyright 2018, Cordite Foundation.
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.
#

# build environment
# usage ./build_env.sh <cordite image tag> <environment>

IMAGE_TAG=${1:-edge}
ENVIRONMENT_SLUG=${2:-dev}
FILE=docker-compose-public.yml

echo -e "\xE2\x9C\x94 $(date) create environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"
set -e

# clean up any old docker-compose
echo docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} down
docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} down

# start NMS (and wait for it to be ready)
# docker login network-map
docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} up -d network-map
until docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} logs network-map | grep -q "io.cordite.networkmap.NetworkMapApp - started"
do
    echo -e "waiting for network-map to start"
    sleep 5
done

# start notaries (and wait for them to be ready)
# docker login cordite
for NOTARY in bootstrap-notary
do
    docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} up -d ${NOTARY}
done
for NOTARY in bootstrap-notary
do
  until docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} logs ${NOTARY} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NOTARY} to start up and register..."
    sleep 5
  done
done

# Pause Notaries but not before downloading their NodeInfo-* and whitelist.txt
for NOTARY in bootstrap-notary
do    
    NODE_NAME=${ENVIRONMENT_SLUG}_${NOTARY}_1
    NODEINFO=$(docker exec ${NODE_NAME} ls | grep nodeInfo-)
    docker cp ${NODE_NAME}:/opt/cordite/${NODEINFO} ${NODEINFO}
    docker cp ${NODE_NAME}:/opt/cordite/whitelist.txt whitelist.txt
    docker exec ${NODE_NAME} rm network-parameters
    docker pause ${NODE_NAME}
done

# Copy Notary NodeInfo-* and whitelist.txt to NMS
NMS_NAME=${ENVIRONMENT_SLUG}_network-map_1
docker cp whitelist.txt ${NMS_NAME}:/mnt/cordite/network-map/db/inputs/whitelist.txt
rm whitelist.txt
echo -e "\xE2\x9C\x94 copied whitelist.txt to ${NMS_NAME}"
for NODEINFO in nodeInfo-*
do
    docker cp ${NODEINFO} ${NMS_NAME}:/mnt/cordite/network-map/db/inputs/validating-notaries/${NODEINFO}
    rm ${NODEINFO}
    echo -e "\xE2\x9C\x94 copied ${NODEINFO} to ${NMS_NAME}"
done

# re-start the notaries
for NOTARY in bootstrap-notary
do
    docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} restart ${NOTARY}
done


# start regional nodes (and wait for them to be ready)
for NODE in emea
do
    docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} up -d ${NODE}
done
for NODE in emea
do
  until docker-compose -f ${FILE} -p ${ENVIRONMENT_SLUG} logs ${NODE} | grep -q "started up and registered"
  do
    echo -e "waiting for ${NODE} to start up and register..."
    sleep 5
  done
done

# test endpoints
for PORT in 8081 
do
    while [[ "$(curl -sSfk -m 5 -o /dev/null -w ''%{http_code}'' https://localhost:${PORT}/api)" != "200" ]]
    do
    echo -e "waiting for ${PORT} to return 200..."
    sleep 5
    done
done

echo -e "\xE2\x9C\x94 $(date) created environment ${ENVIRONMENT_SLUG} with image tag ${IMAGE_TAG}"
