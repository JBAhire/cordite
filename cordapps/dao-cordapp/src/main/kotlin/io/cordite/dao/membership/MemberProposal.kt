/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.membership

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.Proposal
import io.cordite.dao.ProposalKey
import io.cordite.dao.ProposalState
import io.cordite.dao.data.DataHelper
import io.cordite.dao.flows.ChangeMemberFlow
import io.cordite.dao.flows.MakeDaoMembersConsistentWithProposalsFlow
import net.corda.core.contracts.Requirements.using
import net.corda.core.flows.FlowLogic
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable

@CordaSerializable
data class MemberProposal(val member: Party, val daoName: String, val type: Type = Type.NewMember, val proposalKey: ProposalKey = ProposalKey("${type.name}:$daoName:${member.name}")) : Proposal {

  @CordaSerializable
  enum class Type { NewMember, RemoveMember }

  override fun key() = proposalKey

  override fun verifyCreate(state: ProposalState<*>) {
    val expectedSupporters = if (type == Type.NewMember) 2 else 1
    "there should be $expectedSupporters supporters" using (state.supporters.size == expectedSupporters)
    "proposer must be a supporter" using (state.supporters.contains(state.proposer))
  }

  override fun verify(state: ProposalState<*>) {
    if (type == Type.NewMember) {
      "new member must be a supporter" using (state.supporters.contains(member))
    }
    "other supporters must be members" using (state.members.containsAll(state.supporters.filter { it != member }))
  }

  override fun initialSupporters(proposer: Party): Set<Party> {
    return when (type) {
      Type.NewMember -> setOf(proposer, member)
      Type.RemoveMember -> setOf(proposer)
    }

  }

  @Suspendable
  override fun handleAcceptance(proposalState: ProposalState<*>, flowLogic: FlowLogic<*>) {
    val membershipState = flowLogic.subFlow(ChangeMemberFlow(this))
    flowLogic.subFlow(MakeDaoMembersConsistentWithProposalsFlow(daoName, membershipState))
  }

}