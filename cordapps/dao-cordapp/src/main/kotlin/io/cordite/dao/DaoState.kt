/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonTypeInfo
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipModelData
import net.corda.core.contracts.LinearState
import net.corda.core.contracts.UniqueIdentifier
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.Party
import net.corda.core.serialization.CordaSerializable
import java.util.*
import kotlin.reflect.KClass

@CordaSerializable
data class DaoKey(val name: String, val uuid: UUID = UUID.randomUUID()) {
  val uniqueIdentifier: UniqueIdentifier = UniqueIdentifier(name, uuid)
}

@JsonIgnoreProperties("linearId")
data class DaoState(val name: String, val members: Set<Party>, val daoKey: DaoKey = DaoKey(name), val modelDataSet: MutableMap<String, ModelData> = mutableMapOf()) : LinearState {

  override val participants: List<AbstractParty> = members.toList()

  fun <T : ModelData> containsModelData(key: KClass<T>): Boolean {
    return modelDataSet.containsKey(key.qualifiedName)
  }

  @Suppress("UNCHECKED_CAST")
  fun <T : ModelData> get(key: KClass<T>): T? {
    return modelDataSet[key.qualifiedName] as T?
  }

  override val linearId: UniqueIdentifier
    get() = daoKey.uniqueIdentifier

  fun copyWith(newMember: Party): DaoState {
    return DaoState(name, members + newMember, daoKey, modelDataSet)
  }

  fun copyWithout(oldMember: Party): DaoState {
    return DaoState(name, members - oldMember, daoKey, modelDataSet)
  }

  fun copyWith(newModelData: ModelData): DaoState {
    val newModelDataSet = modelDataSet.toMutableMap()
    newModelDataSet.put(newModelData::class.qualifiedName!!, newModelData)
    return DaoState(name, members, daoKey, newModelDataSet)
  }

}

@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
@CordaSerializable
interface ModelData

@CordaSerializable
data class SampleModelData(val someString: String) : ModelData

