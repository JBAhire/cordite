/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.*
import io.cordite.dao.data.DataHelper
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.util.contextLogger
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction

@InitiatingFlow
@StartableByRPC
@StartableByService
class AcceptProposalFlow<T : Proposal>(private val proposalKey: ProposalKey) : FlowLogic<ProposalState<T>>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call(): ProposalState<T> {
    val me = serviceHub.myInfo.legalIdentities.first()

    log.info("$me proposing to accept proposal $proposalKey")

    val inputProposalStateAndRef = findHeadProposalStateFromVault()
    val inputProposalState = inputProposalStateAndRef.state.data
    val inputMembershipStateAndRef = DataHelper.membershipStateAndRefFor(inputProposalState.daoKey, serviceHub)
    val notary = inputMembershipStateAndRef.state.notary

    val (newProposalState, txBuilder) = ProposalContract.generateAcceptProposal(inputProposalStateAndRef, inputMembershipStateAndRef, notary)

    txBuilder.verify(serviceHub)

    val signedTx = serviceHub.signInitialTransaction(txBuilder)

    val flowSessions = newProposalState.participantSet().filter { it != me }.map { initiateFlow(it) }
    log.info("there are ${flowSessions.size} sigs to get...")

    val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

    val finalTx = subFlow(FinalityFlow(fullySignedUnNotarisedTx))
    log.info("proposal: ${proposalKey.name} created.  TxHash: $finalTx")

    // now handle the proposal's post acceptance steps
    newProposalState.proposal.handleAcceptance(newProposalState, this)

    return newProposalState
  }

  private fun findHeadProposalStateFromVault(): StateAndRef<ProposalState<T>> {
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(proposalKey.uniqueIdentifier)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of ProposalStates with name: ${proposalKey.name} found (${vaultPage.totalStatesAvailable})")
      throw IllegalStateException("there should be exactly one ProposalState called: \"${proposalKey.name}\" but there are: ${vaultPage.states.size}")
    }
    @Suppress("UNCHECKED_CAST")
    return vaultPage.states.first() as StateAndRef<ProposalState<T>>
  }

}

@InitiatedBy(AcceptProposalFlow::class)
class AcceptProposalFlowResponder(val creatorSession: FlowSession) : FlowLogic<Unit>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call() {

    val signTransactionFlow = object : SignTransactionFlow(creatorSession, SignTransactionFlow.tracker()) {
      override fun checkTransaction(stx: SignedTransaction) = requireThat {
        // the tx has already been verified and this time we don't have to verify that all dao members are members!
        log.info("accepting proposal - it's been verified")
      }
    }

    val stx = subFlow(signTransactionFlow)

    waitForLedgerCommit(stx.id)
  }
}