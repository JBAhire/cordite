/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.DaoContract
import io.cordite.dao.DaoState
import io.cordite.dao.membership.MembershipContract
import io.cordite.dao.membership.MembershipModelData
import io.cordite.dao.util.contextLogger
import net.corda.core.flows.*
import net.corda.core.identity.Party

@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateDaoFlow(val name: String, val notary : Party): FlowLogic<DaoState>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call(): DaoState {
        log.info("creating new Dao: $name")
        val me = serviceHub.myInfo.legalIdentities.first()

        val (newMembershipState, txBuilder) = MembershipContract.generateCreate(me, name, notary)
        val newDaoState = DaoContract.generateCreate(name, newMembershipState.members, MembershipModelData(newMembershipState.membershipKey, 1), txBuilder)

        txBuilder.verify(serviceHub)

        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        val finalTx = subFlow(FinalityFlow(signedTx))
        log.info("dao: $name created.  TxHash: $finalTx")

        return newDaoState
    }

}