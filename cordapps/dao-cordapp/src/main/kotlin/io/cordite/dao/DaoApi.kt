/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao


import io.cordite.commons.utils.toVertxFuture
import io.cordite.commons.utils.transaction
import io.cordite.dao.data.DataHelper
import io.cordite.dao.flows.*
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.membership.MembershipKey
import io.cordite.dao.membership.MembershipState
import io.cordite.dao.util.contextLogger
import io.vertx.core.Future
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.AttachmentId

// TODO:  Clean up.  See: https://gitlab.com/cordite/cordite/issues/178

interface DaoApi {
  // in principle you _could_ have two DAOs with the same name and different create times...
  // need to come back and fix this...but it's nicer than returning null...
  fun daoInfo(daoName: String): List<DaoState>
  fun daoFor(daoKey: DaoKey): DaoState
  fun listDaos(): List<DaoState>

  fun membershipStateFor(daoKey: DaoKey): MembershipState

//  fun proposalFor(proposalKey: ProposalKey): Future<ProposalState<*>>
//  fun createProposal(proposal: Proposal, daoKey: DaoKey): Future<ProposalKey>
//  fun voteForProposal(proposalKey: ProposalKey): Future<ProposalState<*>>
//  fun acceptProposal(proposalKey: ProposalKey): Future<ProposalState<*>>

  fun createDao(daoName: String, notaryName: CordaX500Name): Future<DaoState>
  fun normalProposalsFor(daoKey: DaoKey): List<ProposalState<NormalProposal>>
  fun newMemberProposalsFor(daoKey: DaoKey): List<ProposalState<MemberProposal>>
  fun modelDataProposalsFor(daoKey: DaoKey): List<ProposalState<ModelDataProposal>>
  fun createProposal(name: String, description: String, daoKey: DaoKey): Future<ProposalState<NormalProposal>>
  fun voteForProposal(proposalKey: ProposalKey): Future<ProposalState<NormalProposal>>
  fun acceptProposal(proposalKey: ProposalKey): Future<ProposalState<NormalProposal>>
  fun createNewMemberProposal(daoName: String, sponsor: Party): Future<ProposalState<MemberProposal>>
  fun createRemoveMemberProposal(daoKey: DaoKey, member: Party): Future<ProposalState<MemberProposal>>
  fun voteForMemberProposal(proposalKey: ProposalKey): Future<ProposalState<MemberProposal>>
  fun acceptNewMemberProposal(proposalKey: ProposalKey, sponsor: Party): Future<ProposalState<MemberProposal>>
  fun acceptRemoveMemberProposal(proposalKey: ProposalKey): Future<ProposalState<MemberProposal>>
  fun requestProposalConsistencyCheckFor(daoKey: DaoKey): Future<Set<ProposalKey>>
  fun createModelDataProposal(name: String, newModelData: ModelData, daoKey: DaoKey): Future<ProposalState<ModelDataProposal>>
  fun voteForModelDataProposal(proposalKey: ProposalKey): Future<ProposalState<ModelDataProposal>>
  fun acceptModelDataProposal(proposalKey: ProposalKey): Future<ProposalState<ModelDataProposal>>

  fun nodeConfig(): String
  fun whitelist(): Map<String, List<AttachmentId>>
}

class DaoApiImpl(val serviceHub: AppServiceHub) : DaoApi {

  companion object {
    private val log = contextLogger()
  }

  private val me = serviceHub.myInfo.legalIdentities.first()

  override fun nodeConfig(): String {
    val sb = StringBuilder()
    sb.appendln("MY NODE INFO:").appendln("--------------------")
    sb.appendln(serviceHub.myInfo)
    sb.appendln("WHITELIST:").appendln("--------------------")
    serviceHub.networkParameters.whitelistedContractImplementations.forEach{
      sb.appendln(it.key)
      it.value.forEach { sb.append("\t").appendln(it) }
    }
    sb.appendln("NOTARIES:").appendln("--------------------")
    serviceHub.networkParameters.notaries.forEach { sb.append(it) }
    log.info(sb.toString())
    return sb.toString()
  }

  override fun whitelist(): Map<String, List<AttachmentId>> {
    return serviceHub.networkParameters.whitelistedContractImplementations
  }

  // in principle you _could_ have two DAOs with the same name and different create times...
  // need to come back and fix this...but it's nicer than returning null...
  override fun daoInfo(daoName: String): List<DaoState> {
    return serviceHub.transaction {
      DataHelper.daoStatesFor(daoName, serviceHub)
    }
  }

  override fun daoFor(daoKey: DaoKey): DaoState {
    return serviceHub.transaction {
      DataHelper.daoStateFor(daoKey, serviceHub)
    }
  }

  override fun listDaos(): List<DaoState> {
    return serviceHub.transaction {
      DataHelper.daoStates(serviceHub)
    }
  }

  override fun membershipStateFor(daoKey: DaoKey): MembershipState {
    return serviceHub.transaction {
      DataHelper.membershipStateFor(daoKey, serviceHub)
    }
  }

  override fun createDao(daoName: String, notaryName : CordaX500Name): Future<DaoState> {
    log.info("creating new dao: $daoName")
    val notary = findNotary(notaryName)
    return serviceHub.startFlow(CreateDaoFlow(daoName,notary)).returnValue.toVertxFuture()
  }

  // surely we can refactor the below...
  override fun newMemberProposalsFor(daoKey: DaoKey): List<ProposalState<MemberProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  override fun normalProposalsFor(daoKey: DaoKey): List<ProposalState<NormalProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  override fun modelDataProposalsFor(daoKey: DaoKey): List<ProposalState<ModelDataProposal>> {
    return serviceHub.transaction {
      DataHelper.proposalStatesFor(daoKey, serviceHub)
    }
  }

  override fun createProposal(name: String, description: String, daoKey: DaoKey): Future<ProposalState<NormalProposal>> {
    log.info("creating new proposal: $name")
    val flowFuture = serviceHub.startFlow(CreateProposalFlow(NormalProposal(name, description), daoKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun createNewMemberProposal(daoName: String, sponsor: Party): Future<ProposalState<MemberProposal>> {
    log.info("proposing me: ${serviceHub.myInfo.legalIdentities.first().name} to be a member of $daoName sponsored by $sponsor")
    val flowFuture = serviceHub.startFlow(SponsorProposalFlow(CreateSponsorAction(MemberProposal(me, daoName), daoName), sponsor)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun createRemoveMemberProposal(daoKey: DaoKey, member: Party): Future<ProposalState<MemberProposal>> {
    log.info("proposing to remove: $member from ${daoKey.name}")
    val flowFuture = serviceHub.startFlow(CreateProposalFlow(MemberProposal(member, daoKey.name, MemberProposal.Type.RemoveMember), daoKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun acceptNewMemberProposal(proposalKey: ProposalKey, sponsor: Party): Future<ProposalState<MemberProposal>> {
    log.info("attempting to accept my membership $proposalKey, sponsored by $sponsor")

    val flowFuture = serviceHub.startFlow(SponsorProposalFlow(AcceptSponsorAction<MemberProposal>(proposalKey), sponsor)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun acceptRemoveMemberProposal(proposalKey: ProposalKey): Future<ProposalState<MemberProposal>> {
    log.info("attempting to accept membership removal of $proposalKey")

    val flowFuture = serviceHub.startFlow(AcceptProposalFlow<MemberProposal>(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun voteForMemberProposal(proposalKey: ProposalKey): Future<ProposalState<MemberProposal>> {
    log.info("voting for new member proposal: $proposalKey")
    val flowFuture = serviceHub.startFlow(VoteForProposalFlow<MemberProposal>(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun voteForProposal(proposalKey: ProposalKey): Future<ProposalState<NormalProposal>> {
    log.info("voting for proposal: $proposalKey")
    val flowFuture = serviceHub.startFlow(VoteForProposalFlow<NormalProposal>(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun acceptProposal(proposalKey: ProposalKey): Future<ProposalState<NormalProposal>> {
    log.info("accepting proposal: $proposalKey")

    val flowFuture = serviceHub.startFlow(AcceptProposalFlow<NormalProposal>(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun requestProposalConsistencyCheckFor(daoKey: DaoKey): Future<Set<ProposalKey>> {
    log.info("asking random dao member to add me to any proposals, for dao $daoKey, that they have which i do not belong to")

    val flowFuture = serviceHub.startFlow(RequestProposalConsistencyCheckFlow(daoKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun createModelDataProposal(name: String, newModelData: ModelData, daoKey: DaoKey): Future<ProposalState<ModelDataProposal>> {
    log.info("creating new model data proposal: $name")
    val flowFuture = serviceHub.startFlow(CreateProposalFlow(ModelDataProposal(name, newModelData), daoKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun voteForModelDataProposal(proposalKey: ProposalKey): Future<ProposalState<ModelDataProposal>> {
    log.info("voting for proposal: $proposalKey")
    val flowFuture = serviceHub.startFlow(VoteForProposalFlow<ModelDataProposal>(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  override fun acceptModelDataProposal(proposalKey: ProposalKey): Future<ProposalState<ModelDataProposal>> {
    log.info("accepting proposal: $proposalKey")
    val flowFuture = serviceHub.startFlow(AcceptProposalFlow<ModelDataProposal>(proposalKey)).returnValue
    return flowFuture.toVertxFuture()
  }

  private fun findNotary(notary: CordaX500Name): Party {
    return serviceHub.networkMapCache.getNotary(notary) ?: throw RuntimeException("could not find notary $notary")
  }
}