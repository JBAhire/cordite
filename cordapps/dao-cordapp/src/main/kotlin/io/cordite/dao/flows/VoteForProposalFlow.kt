/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.*
import io.cordite.dao.data.DataHelper
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.util.contextLogger
import net.corda.core.contracts.StateAndRef
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction
import org.bouncycastle.asn1.x500.style.RFC4519Style.name

@InitiatingFlow
@StartableByRPC
@StartableByService
class VoteForProposalFlow<T : Proposal>(val proposalKey: ProposalKey) : FlowLogic<ProposalState<T>>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call(): ProposalState<T> {
    val me = serviceHub.myInfo.legalIdentities.first()

    log.info("${me.name} voting for ${proposalKey}")

    val inputProposalStateAndRef = findHeadProposalStateFromVault()
    val inputProposalState = inputProposalStateAndRef.state.data
    val membershipTxStateAndRef = DataHelper.membershipStateAndRefFor(inputProposalState.daoKey, serviceHub)
    val notary = membershipTxStateAndRef.state.notary

    val (newProposalState, txBuilder) = ProposalContract.generateVoteForProposal(inputProposalStateAndRef, me, membershipTxStateAndRef.state.data, notary)

    txBuilder.verify(serviceHub)

    val signedTx = serviceHub.signInitialTransaction(txBuilder)

    val flowSessions = newProposalState.participantSet().filter { it != me }.map { initiateFlow(it) }
    log.info("there are ${flowSessions.size} sigs to get...")

    val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

    val finalTx = subFlow(FinalityFlow(fullySignedUnNotarisedTx))
    log.info("proposal: $name created.  TxHash: $finalTx")

    return newProposalState
  }

  private fun findHeadProposalStateFromVault(): StateAndRef<ProposalState<T>> {
    val vaultPage = serviceHub.vaultService.queryBy(ProposalState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(proposalKey.uniqueIdentifier)))
    if (vaultPage.states.size != 1) {
      log.warn("incorrect number of ProposalStates with name: ${proposalKey.name} found (${vaultPage.totalStatesAvailable}")
      throw IllegalStateException("there should be exactly one ProposalState called: \"${proposalKey.name}\" but there are: ${vaultPage.states.size}")
    }
    @Suppress("UNCHECKED_CAST")
    return vaultPage.states.first() as StateAndRef<ProposalState<T>>
  }

}

@InitiatedBy(VoteForProposalFlow::class)
class VoteForProposalFlowResponder(val creatorSession: FlowSession) : FlowLogic<Unit>() {

  companion object {
    private val log = contextLogger()
  }

  @Suspendable
  override fun call() {

    log.info("*********************************")

    val signTransactionFlow = object : SignTransactionFlow(creatorSession, SignTransactionFlow.tracker()) {
      override fun checkTransaction(stx: SignedTransaction) = requireThat {
        // the tx has already been verified but we need to confirm that all the dao state members are signers

        val transaction = stx.toLedgerTransaction(serviceHub, false)
        val commands = transaction.commandsOfType(ProposalContract.Commands.VoteForProposal::class.java)
        val outputStates = transaction.outputStates

        // TODO: not sure how to marry up commands to dao states for the mo so presume one command and output state https://gitlab.com/cordite/cordite/issues/281

        "there should be exactly one command" using (commands.size == 1)
        "there should be exactly one output state" using (outputStates.size == 1)

        val signers = commands.first().signers
        val proposalState = outputStates.first() as ProposalState<*>
        val daoKey = proposalState.daoKey
        val me = serviceHub.myInfo.legalIdentities.first()

        if (proposalState.proposal is MemberProposal) {
          val newMemberProposal = proposalState.proposal
          if (newMemberProposal.member == me) {
            log.info("proposed new member is me!  accepting proposal")
            return
          }
        }

        val vaultPage = serviceHub.vaultService.queryBy(DaoState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(daoKey.uniqueIdentifier)))
        if (vaultPage.states.size != 1) {
          log.warn("incorrect number of DAOs with name: ${daoKey.name} found (${vaultPage.totalStatesAvailable}")
          throw IllegalStateException("there should be exactly one Dao with key: \"$daoKey\" but there are: ${vaultPage.states.size}")
        }
        val daoTxStateAndRef = vaultPage.states.first()
        val daoState = daoTxStateAndRef.state.data

        "all dao members must be proposal members" using (proposalState.members == daoState.members)
        "all dao members must be signers" using (signers.containsAll(daoState.members.map { it.owningKey }))

        log.info("all dao members are signers - accepting proposal")
      }
    }

    val stx = subFlow(signTransactionFlow)

    waitForLedgerCommit(stx.id)
  }
}