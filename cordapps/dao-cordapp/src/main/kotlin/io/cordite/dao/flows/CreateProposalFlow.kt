/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.flows

import co.paralleluniverse.fibers.Suspendable
import io.cordite.dao.*
import io.cordite.dao.data.DataHelper
import io.cordite.dao.membership.MemberProposal
import io.cordite.dao.util.contextLogger
import net.corda.core.contracts.requireThat
import net.corda.core.flows.*
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.transactions.SignedTransaction

/*
we really only have four ways of handling this:

 1. Put _everything_ on the DaoState
    * horrific due to concurrency issues
 2. Include an unspent DaoState in every tx that creates a proposal or votes
    * this is logically equivalent to 1
    * no concurrency - but then maybe we don't care
    * on the plus side it's very deterministic and simple
 3. Same as above but don't spend it - just refer and check it's valid
    * think this uses oracles
    * not very deterministic - is it idempotent
    * not supported by corda and no plans to support
 4. Proposals just "have to have" Dao members as signers
    * if they don't, we don't care about them
    * if they do, we have some concurrency gains
    * at a cost of having to maintain the membership lists

The simplest and safest is 1 or 2, but there are concurrency issues.  The 4th is the other option.
Really easy, but quite a bit of work to do at membership change time.  It's safe work, though, as you
would do it in the same tx as making the membership change.

Going with 4/. fairly arbitrarily for the mo.
 */


@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateProposalFlow<T : Proposal>(val proposal: T, private val daoKey: DaoKey): FlowLogic<ProposalState<T>>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call(): ProposalState<T> {
        log.info("creating new proposal: ${proposal.key().name}")
        val me = serviceHub.myInfo.legalIdentities.first()

        val membershipStateAndRef = DataHelper.membershipStateAndRefFor(daoKey, serviceHub)

        val notary = membershipStateAndRef.state.notary
        val membershipState = membershipStateAndRef.state.data

        val (newProposalState, txBuilder) = ProposalContract.generateCreateProposal(proposal, daoKey, membershipState, me, notary)
        log.info("proposal state created with ${newProposalState.members.size} members")

        txBuilder.verify(serviceHub)

        val signedTx = serviceHub.signInitialTransaction(txBuilder)

        val flowSessions = newProposalState.participantSet().filter{ it != me }.map{ initiateFlow(it) }
        log.info("there are ${flowSessions.size} sigs to get...")

        val fullySignedUnNotarisedTx = subFlow(CollectSignaturesFlow(signedTx, flowSessions, CollectSignaturesFlow.tracker()))

        val finalTx = subFlow(FinalityFlow(fullySignedUnNotarisedTx))
        log.info("proposal: ${proposal.key().name} created.  TxHash: $finalTx")

        return newProposalState
    }

}

@InitiatedBy(CreateProposalFlow::class)
class CreateProposalFlowResponder(val creatorSession: FlowSession) : FlowLogic<Unit>() {

    companion object {
        private val log = contextLogger()
    }

    @Suspendable
    override fun call() {

        val signTransactionFlow = object : SignTransactionFlow(creatorSession, SignTransactionFlow.tracker()) {
            override fun checkTransaction(stx: SignedTransaction) = requireThat {
                // the tx has already been verified but we need to confirm that all the dao state members are signers

                val transaction = stx.toLedgerTransaction(serviceHub, false)
                val commands = transaction.commandsOfType(ProposalContract.Commands.CreateProposal::class.java)
                val outputStates = transaction.outputStates

                // TODO: not sure how to marry up commands to dao states for the mo so presume one command and output state https://gitlab.com/cordite/cordite/issues/281

                "there should be exactly one command" using (commands.size == 1)
                "there should be exactly one output state" using (outputStates.size == 1)

                val signers = commands.first().signers
                val proposalState = outputStates.first() as ProposalState<*>
                val daoKey = proposalState.daoKey
                val me = serviceHub.myInfo.legalIdentities.first()

                if (proposalState.proposal is MemberProposal) {
                    val newMemberProposal = proposalState.proposal
                    if (newMemberProposal.member == me) {
                        log.info("proposed new member is me!  accepting proposal creation")
                        return
                    }
                }

                // must be a member of the dao, so check dao members
                val vaultPage = serviceHub.vaultService.queryBy(DaoState::class.java, QueryCriteria.LinearStateQueryCriteria(linearId = listOf(daoKey.uniqueIdentifier)))
                if (vaultPage.states.size != 1) {
                    log.warn("incorrect number of DAOs with name: ${daoKey.name} found (${vaultPage.totalStatesAvailable}")
                    throw IllegalStateException("there should be exactly one Dao with key: \"$daoKey\" but there are: ${vaultPage.states.size}")
                }
                val daoTxStateAndRef = vaultPage.states.first()
                val daoState = daoTxStateAndRef.state.data

                "all dao members must be proposal members" using ( proposalState.members == daoState.members )
                "all dao members must be signers" using ( signers.containsAll(daoState.members.map { it.owningKey }))

                log.info("all dao members are signers - accepting proposal")
            }
        }

        val stx = subFlow(signTransactionFlow)

        waitForLedgerCommit(stx.id)
    }
}