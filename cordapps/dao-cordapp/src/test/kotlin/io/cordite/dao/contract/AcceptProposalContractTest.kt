/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.contract


import io.cordite.dao.*
import io.cordite.dao.membership.MEMBERSHIP_CONTRACT_ID
import io.cordite.dao.membership.MembershipContract
import io.cordite.test.utils.*
import net.corda.testing.node.ledger
import org.junit.Test

// NB we cannot really check that all the DaoState members are participants in the current scheme, so members
// must check this in the responder flows - discussion in CreateProposalFlow
class AcceptProposalContractTest {

  private val members = setOf(proposerParty, voterParty, newMemberParty)
  private val membershipState = membershipState(members = members)
  private val initialProposalState = proposalState(members = members, supporters = setOf(proposerParty, voterParty))
  private val outputProposalState = initialProposalState.copyWithNewLifecycleState(ProposalLifecycle.ACCEPTED)

  @Test
  fun `happy path should verify`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        verifies()
      }
    }
  }

  @Test
  fun `there should be one input proposal`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("There should be one input proposal")
      }
    }
  }


  @Test
  fun `there should be one output state`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("There should be one output proposal")
      }
    }
  }

  @Test
  fun `proposers should be the same`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposalState, proposer = daoParty))
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("proposers should be the same")
      }
    }
  }

  @Test
  fun `no members should be removed`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, proposalCopy(initialProposalState, members = setOf(proposerParty)))
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("no members should be removed")
      }
    }
  }

  @Test
  fun `there must be a corresponding MembershipState`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("there must be a corresponding MembershipState")
      }
    }
  }

  @Test
  fun `the proposal state members must be the same as the membership state's members`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(MEMBERSHIP_CONTRACT_ID, membershipState.copyWithout(voterParty))
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("the proposal state members must be the same as the membership state's members")
      }
    }
  }

  @Test
  fun `the proposal must have enough support`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState.copyWithoutSupporter(proposerParty))
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, initialProposalState.copyWithoutSupporter(proposerParty))
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("the proposal must have enough support")
      }
    }
  }

  @Test
  fun `proposer must be signer`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.filter { it != proposerParty }.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("proposer must be signer")
      }
    }
  }

  @Test
  fun `supporters must be members`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState.copyWithNewSupporter(member1Party))
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState.copyWithNewSupporter(member1Party))
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("supporters must be members")
      }
    }
  }

  @Test
  fun `all members must be signers`() {
    ledgerServices.ledger {
      transaction {
        input(PROPOSAL_CONTRACT_ID, initialProposalState)
        input(MEMBERSHIP_CONTRACT_ID, membershipState)
        output(PROPOSAL_CONTRACT_ID, outputProposalState)
        output(MEMBERSHIP_CONTRACT_ID, membershipState)
        command(members.filter { it != voterParty }.map { it.owningKey }, ProposalContract.Commands.AcceptProposal())
        command(members.map { it.owningKey }, MembershipContract.Commands.ReferToMembershipState())
        `fails with`("members must be signers")
      }
    }
  }

}