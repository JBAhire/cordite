/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dao.integration

import io.bluebank.braid.client.BraidProxyClient
import io.cordite.dao.*
import io.cordite.dao.util.contextLogger
import io.cordite.test.utils.*
import io.vertx.ext.unit.junit.VertxUnitRunner
import junit.framework.Assert.assertTrue
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.testing.node.MockNetwork
import net.corda.testing.node.StartedMockNode
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import org.junit.runner.RunWith


class TestNode(node: StartedMockNode, braidPortHelper: BraidPortHelper) {

  private val braidClient: BraidProxyClient

  val party: Party = node.info.legalIdentities.first()
  val daoApi: DaoApi

  init {

    braidClient = BraidClientHelper.braidClient(braidPortHelper.portForParty(party), "daoservice", "localhost")
    daoApi = braidClient.bind(DaoApi::class.java)
  }

  fun shutdown() {
    braidClient.close()
  }

}

@RunWith(VertxUnitRunner::class)
class DaoIntegrationTest {

  companion object {
    private val log = contextLogger()
    private val casaDaoBase = "casaDao"
    private val proposalBase = "proposal"

    private lateinit var network: MockNetwork

    private val braidPortHelper = BraidPortHelper()

    private lateinit var proposer: TestNode
    private lateinit var newMember: TestNode
    private lateinit var anotherMember: TestNode
    private lateinit var notaryName: CordaX500Name

    private var salt = 0

    @BeforeClass
    @JvmStatic
    fun setup() {

      braidPortHelper.setSystemPropertiesFor(proposerName, newMemberName, anotherMemberName)
      network = MockNetwork(cordappPackages = daoCordappPackages())

      proposer = TestNode(network.createPartyNode(proposerName), braidPortHelper)
      newMember = TestNode(network.createPartyNode(newMemberName), braidPortHelper)
      anotherMember = TestNode(network.createPartyNode(anotherMemberName), braidPortHelper)

      network.runNetwork()

      notaryName = network.defaultNotaryIdentity.name
    }

    @AfterClass
    @JvmStatic
    fun tearDown() {
      proposer.shutdown()
      newMember.shutdown()
      anotherMember.shutdown()
      try {
        network.stopNodes()
      } catch (e: NullPointerException) {
        log.info(e.message)
      }
    }

  }

  private val saltedDaoName = casaDaoBase + salt++
  private val saltedProposalName = proposalBase + salt++

  @Test
  fun `should be able to get whitelist`() {
    val config = proposer.daoApi.nodeConfig()
    assertTrue("config should contain notary info", config.contains("NotaryInfo(identity=O=Notary Service, L=Zurich, C=CH, validating=true)"))
    assertTrue("config should contain node info", config.contains("NodeInfo(addresses=[mock.node:1000], legalIdentitiesAndCerts=[O=Proposer, L=London, C=GB], platformVersion=1"))
  }

  @Test
  fun `should be able to create a dao`() {
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to create another dao in the same node`() {
    // because the salt changes for each test run this is actually a different dao..
    createDaoWithName(saltedDaoName, notaryName)
  }

  @Test
  fun `should be able to get list of daos`() {
    val initialDaoCount = proposer.daoApi.listDaos().size
    createDaoWithName(saltedDaoName, notaryName)
    val newDaoCount = proposer.daoApi.listDaos().size
    Assert.assertEquals("incorrect number of daos", initialDaoCount + 1, newDaoCount)
  }

  @Test
  fun `should be able to add a new member`() {
    createDaoWithName(saltedDaoName, notaryName)
    val proposal = run(network) { newMember.daoApi.createNewMemberProposal(saltedDaoName, proposer.party) }
    Assert.assertEquals("should be 2 supporters", 2, proposal.supporters.size)
    Assert.assertTrue("proposer should be a supporter", proposal.supporters.contains(proposer.party))
    Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMember.party))

    // accept member proposal
    val acceptedProposal = run(network) { newMember.daoApi.acceptNewMemberProposal(proposal.proposal.key(), proposer.party) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)

    assertDaoStateContainsMembers(getDaoWithRetry(newMember), proposer.party, newMember.party)
  }

  @Test
  fun `should be able to add two members`() {
    createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
  }

  @Test
  fun `should be able to create, vote for and accept new proposal`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    createAndAcceptProposalWithName(saltedProposalName, proposer, daoState, newMember)
  }

  @Test
  fun `new member should receive proposals`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    createProposal(saltedProposalName, proposer, daoState)
    addMemberToDao(newMember, proposer, saltedDaoName)
    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("should have correct proposal", saltedProposalName, proposals.first().name)
  }

  @Test
  fun `removing member from dao should remove them from proposals`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    createProposal(saltedProposalName, proposer, daoState)
    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)

    val removeProposal = run(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party) }

    Assert.assertEquals("should be 1 supporters", 1, removeProposal.supporters.size)
    Assert.assertTrue("proposer should be a supporter", removeProposal.supporters.contains(proposer.party))

    // other member approves
    run(network) { anotherMember.daoApi.voteForMemberProposal(removeProposal.proposal.proposalKey) }

    val acceptedProposal = run(network) { proposer.daoApi.acceptRemoveMemberProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

    val currentProposals = proposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
    Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
  }

  @Test
  fun `removing member from dao should remove them from proposal supporters`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)
    addMemberToDao(anotherMember, proposer, saltedDaoName, newMember)
    val proposal = createProposal(saltedProposalName, proposer, daoState)

    // support the proposal
    forTheLoveOfGodIgnoreThisBit()
    run(network) { newMember.daoApi.voteForProposal(proposal.proposal.proposalKey) }

    val proposals = newMember.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("new member should now have the proposal", 1, proposals.size)
    Assert.assertEquals("there should be 3 members", 3, proposals.first().members.size)
    Assert.assertTrue("new member should be supporter", proposals.first().supporters.contains(newMember.party))

    val removeProposal = run(network) { proposer.daoApi.createRemoveMemberProposal(daoState.daoKey, newMember.party) }

    Assert.assertEquals("should be 1 supporters", 1, removeProposal.supporters.size)
    Assert.assertTrue("proposer should be a supporter", removeProposal.supporters.contains(proposer.party))

    // other member approves
    run(network) { anotherMember.daoApi.voteForMemberProposal(removeProposal.proposal.proposalKey) }

    val acceptedProposal = run(network) { proposer.daoApi.acceptRemoveMemberProposal(removeProposal.proposal.key()) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)

    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party, anotherMember.party)

    val currentProposals = proposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should still be 1 proposal", 1, currentProposals.size)
    Assert.assertEquals("there should now be 2 members", 2, currentProposals.first().members.size)
    Assert.assertFalse("new member should not now be a supporter", currentProposals.first().supporters.contains(newMemberParty))
  }

  @Test
  fun `should be able to create a metering model data for a dao`() {
    val daoState = createDaoWithName(saltedDaoName, notaryName)
    addMemberToDao(newMember, proposer, saltedDaoName)

    Assert.assertFalse(daoState.containsModelData(SampleModelData::class))

    val newModelData = SampleModelData("initial")
    val proposalState = run(network) { proposer.daoApi.createModelDataProposal("fiddle with metering model data", newModelData, daoState.daoKey) }

    run(network) { newMember.daoApi.voteForModelDataProposal(proposalState.proposal.proposalKey) }

    val proposals = proposer.daoApi.modelDataProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have 2 supporters", 2, proposals.first().supporters.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposer.party, newMember.party)))

    val acceptedProposal = run(network) { proposer.daoApi.acceptModelDataProposal(proposalState.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)
    val newDaoState = proposer.daoApi.daoFor(acceptedProposal.daoKey)
    Assert.assertEquals("dao should contain metering model data", newModelData, newDaoState.get(newModelData::class))
  }

  private fun getDaoWithRetry(testNode: TestNode): List<DaoState> {
    (1..5).forEach {
      log.info("trying to get dao list")
      val daos = testNode.daoApi.daoInfo(saltedDaoName)
      if (daos.isNotEmpty()) {
        log.info("phew - daos returned")
        return daos
      }
      log.info("dao state not arrived yet - snoozing")
      Thread.sleep(100)
    }
    throw RuntimeException("Unable to get daos from node - are you sure this is a race condition?")
  }

  private fun createDaoWithName(daoName: String, notaryName: CordaX500Name): DaoState {
    Assert.assertEquals("there should be no casa dao at the beginning", 0, proposer.daoApi.daoInfo(daoName).size)
    val daoState = run(network) { proposer.daoApi.createDao(daoName, notaryName) }
    forTheLoveOfGodIgnoreThisBit()
    assertDaoStateContainsMembers(getDaoWithRetry(proposer), proposer.party)
    return daoState
  }

  private fun addMemberToDao(newMemberNode: TestNode, proposerNode: TestNode, daoName: String, vararg extraSigners: TestNode) {
    val proposal = run(network) { newMemberNode.daoApi.createNewMemberProposal(daoName, proposerNode.party) }

    Assert.assertEquals("should be 2 supporters", 2, proposal.supporters.size)
    Assert.assertTrue("proposer should be a supporter", proposal.supporters.contains(proposerNode.party))
    Assert.assertTrue("member should be a supporter", proposal.supporters.contains(newMemberNode.party))

    var numberOfSupporters = 2
    extraSigners.forEach {
      val postVote = run(network) { it.daoApi.voteForMemberProposal(proposal.proposal.proposalKey) }
      Assert.assertEquals("there should be three supporters", ++numberOfSupporters, postVote.supporters.size)
    }

    // accept member proposal
    val acceptedProposal = run(network) { newMember.daoApi.acceptNewMemberProposal(proposal.proposal.key(), proposerNode.party) }
    Assert.assertEquals("proposal should be accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)

    assertDaoStateContainsMembers(getDaoWithRetry(newMemberNode), proposerNode.party, newMemberNode.party, *extraSigners.map { it.party }.toTypedArray())
  }

  private fun createAndAcceptProposalWithName(proposalName: String, proposalProposer: TestNode, daoState: DaoState, vararg supporters: TestNode) {
    val proposal = createProposal(proposalName, proposalProposer, daoState)

    supporters.forEach {
      run(network) { it.daoApi.voteForProposal(proposal.proposal.proposalKey) }
    }

    val proposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)

    Assert.assertEquals("there should only be one proposal", 1, proposals.size)
    Assert.assertEquals("the proposal should have ${supporters.size + 1} supporters", supporters.size + 1, proposals.first().supporters.size)
    Assert.assertTrue("the proposal should have all supporters", proposals.first().supporters.containsAll(setOf(proposalProposer.party, *supporters.map { it.party }.toTypedArray())))

    val acceptedProposal = run(network) { proposalProposer.daoApi.acceptProposal(proposal.proposal.proposalKey) }
    Assert.assertEquals("proposal should have been accepted", ProposalLifecycle.ACCEPTED, acceptedProposal.lifecycleState)
  }

  private fun createProposal(proposalName: String, proposalProposer: TestNode, daoState: DaoState): ProposalState<NormalProposal> {
    val proposal = run(network) { proposalProposer.daoApi.createProposal(proposalName, "some description", daoState.daoKey) }

    val origProposals = proposalProposer.daoApi.normalProposalsFor(daoState.daoKey)
    Assert.assertEquals("there should only be one proposal", 1, origProposals.size)
    Assert.assertEquals("the proposal should have one supporter", 1, origProposals.first().supporters.size)
    Assert.assertTrue("proposer should be supporter", origProposals.first().supporters.containsAll(setOf(proposalProposer.party)))
    Assert.assertEquals("keys should be the same", proposal.proposal.proposalKey, origProposals.first().proposal.proposalKey)
    return proposal
  }
}
