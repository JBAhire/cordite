/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl.corda.token

import co.paralleluniverse.fibers.Suspendable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import io.cordite.commons.utils.transaction
import io.cordite.dgl.corda.crud.CrudContract
import io.cordite.dgl.corda.crud.CrudCreateFlow
import net.corda.core.contracts.*
import net.corda.core.flows.FlowLogic
import net.corda.core.flows.InitiatingFlow
import net.corda.core.flows.StartableByRPC
import net.corda.core.flows.StartableByService
import net.corda.core.identity.AbstractParty
import net.corda.core.identity.CordaX500Name
import net.corda.core.identity.Party
import net.corda.core.node.AppServiceHub
import net.corda.core.node.services.queryBy
import net.corda.core.node.services.vault.PageSpecification
import net.corda.core.node.services.vault.QueryCriteria
import net.corda.core.schemas.MappedSchema
import net.corda.core.schemas.PersistentState
import net.corda.core.schemas.QueryableState
import net.corda.core.serialization.CordaSerializable
import net.corda.core.transactions.SignedTransaction
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

class TokenType : CrudContract<TokenType.State>(TokenType.State::class) {
  companion object {
    val CONTRACT_ID : ContractClassName = TokenType::class.java.name
  }

  interface TokenTypeCommands {
    class Create : TypeOnlyCommandData(), TokenTypeCommands
  }

  object TokenTypeSchema

  object TokenTypeSchemaV1 : MappedSchema(TokenTypeSchema::class.java, 1, listOf(TokenTypeSchemaV1.PersistedTokenType::class.java)) {
    @Entity
    @Table(name = "CORDITE_TOKEN_TYPE")
    class PersistedTokenType(
        @Column(name = "symbol")
        val symbol: String,
        @Column(name = "exponent")
        val exponent: Int
    ) : PersistentState() {
      constructor() : this("", 0)
    }
  }

  @JsonIgnoreProperties(ignoreUnknown = true)
  @CordaSerializable
  data class Descriptor(
      val symbol: String,
      val exponent: Int,
      val issuerName: CordaX500Name
  ) : TokenizableAssetInfo {
    companion object {
      const val SEPARATOR = ':'
      fun parse(uri: String) : Descriptor {
        val parts = uri.split(SEPARATOR)
        if (parts.size != 3) throw RuntimeException("invalid token type descriptor $uri")
        return Descriptor(parts[0], parts[1].toInt(), CordaX500Name.parse(parts[2]))
      }
    }
    init {
      if (symbol.contains(SEPARATOR)) {
        throw RuntimeException("token descriptor cannot contain $SEPARATOR")
      }
    }
    override val displayTokenSize: BigDecimal
      get() = BigDecimal.ONE.scaleByPowerOfTen(-exponent)

    val uri : String get() = "$symbol$SEPARATOR$exponent$SEPARATOR$issuerName"
  }


  @JsonIgnoreProperties(ignoreUnknown = true)
  data class State(val descriptor: Descriptor, val issuer: Party, override val linearId: UniqueIdentifier = UniqueIdentifier(externalId = descriptor.symbol))
    : LinearState, QueryableState {

    constructor(symbol: String, exponent: Int, issuer: Party) : this(Descriptor(symbol, exponent, issuer.name), issuer)

    override val participants: List<AbstractParty> = listOf(issuer)
    override fun generateMappedObject(schema: MappedSchema): PersistentState {
      return when (schema) {
        is TokenTypeSchemaV1 -> {
          TokenTypeSchemaV1.PersistedTokenType(symbol = this.descriptor.symbol, exponent = descriptor.exponent)
        }
        else -> {
          throw RuntimeException("unknown schema version ${schema.version}")
        }
      }
    }

    override fun supportedSchemas() = listOf(TokenTypeSchemaV1)
  }
}

@InitiatingFlow
@StartableByRPC
@StartableByService
class CreateTokenTypeFlow(private val symbol: String, private val exponent: Int, private val notary: Party) : FlowLogic<SignedTransaction>() {
  @Suspendable
  override fun call(): SignedTransaction {
    val state = TokenType.State(symbol, exponent, ourIdentity)
    return subFlow(CrudCreateFlow(listOf(state), TokenType.CONTRACT_ID, notary))
  }
}

fun AppServiceHub.listAllTokenTypes(page: Int, pageSize: Int): List<TokenType.State> {
  return transaction {
    val res = vaultService
        .queryBy<TokenType.State>(QueryCriteria.VaultQueryCriteria(
            contractStateTypes = setOf(TokenType.State::class.java)),
            paging = PageSpecification(pageNumber = page, pageSize = pageSize))
        .states.map { it.state.data }
    res
  }
}

