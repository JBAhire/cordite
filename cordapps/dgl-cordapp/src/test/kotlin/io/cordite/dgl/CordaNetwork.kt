/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite.dgl

import net.corda.core.identity.CordaX500Name
import net.corda.testing.driver.*

class CordaNetwork(private val cordaStartingPort: Int = 5005, private val webStartingPort: Int = 8080) {
  companion object {
    @JvmStatic
    fun main(args: Array<String>) {
      CordaNetwork(5005).withCluster {
        readLine()
      }
    }

    fun createCordaNet(): CordaNetwork {
      return CordaNetwork()
    }
  }

  fun withCluster(callback: DriverDSL.() -> Unit) {
    val parties = listOf("PartyA")

    // setup braid ports per party
    val systemProperties = setupWebPortsPerParty(parties, webStartingPort)

    println("Starting cluster with Corda base port $cordaStartingPort and web base port $webStartingPort")
    driver(DriverParameters(isDebug = false,
        startNodesInProcess = true,
        portAllocation = PortAllocation.Incremental(cordaStartingPort),
        systemProperties = systemProperties,
        extraCordappPackagesToScan = listOf("net.corda.finance", "io.bluebank.braid.corda.integration.cordapp"))) {

      // start up the controller and all the parties
      val nodeHandles = startupNodes()

      // run the rest of the programme
      callback()

      nodeHandles.map { it.stop() }
    }
  }

  private fun DriverDSL.startupNodes(): List<NodeHandle> {
    return listOf(startNode(providedName = CordaX500Name("NetworkMapAndNotary", "London", "GB")))
        .map { it.get() }
  }

  private fun setupWebPortsPerParty(parties: List<String>, braidStartingPort: Int): Map<String, String> {
    val result = with(System.getProperties()) {
      parties
          .mapIndexed { idx, party -> "braid.$party.port" to (idx + braidStartingPort).toString() }
          .toMap()
    }
    result.forEach {
      System.setProperty(it.key, it.value)
    }
    return result
  }
}
