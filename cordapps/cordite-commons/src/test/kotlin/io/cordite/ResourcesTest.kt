/**
 *   Copyright 2018, Cordite Foundation.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package io.cordite

import io.cordite.commons.utils.Resources
import org.junit.Assert.*
import org.junit.Test

class ResourcesTest {
  @Test
  fun `that we can read a text resource`() {
    val text = Resources.loadResourceAsString("test-resource.json")
    assertEquals("true", text)
  }

  @Test(expected = IllegalArgumentException::class)
  fun `that reading a resource that doesn't exist fails`() {
    Resources.loadResourceAsString("resource-not-there")
  }
}